angular.module('phonelist', []).controller('PhoneListInit', function ($scope) {
	$scope.phones = [
		{ "name": "name1", "snippet": "snippet1", "age": 0 },
		{ "name": "name2", "snippet": "snippet2", "age": 2 },
		{ "name": "name3", "snippet": "snippet3", "age": 1 }
	];

	// $scope.orderProp = "age";
});
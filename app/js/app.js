var angmodule = angular.module('phonelist', ['ngRoute']);
angmodule.config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/phones', {
      templateUrl: 'phone-list.html',
      controller: 'PhoneListCtrl'
    })
    .when('/phones/:phoneId', {
      templateUrl: 'phone-detail.html',
      controller: 'PhoneDetailCtrl'
    })
    .otherwise({
      redirectTo: '/phones'
    });
}]);

function PhoneListCtrl($scope, $http) {
  $http.get('http://localhost:17558/api/angularjs?img').success(function(data) {
    alert(data);
    $scope.phones = eval(data);
  });
}

function PhoneDetailCtrl($scope, $routeParams) {
  $scope.phoneId = $routeParams.phoneId;
}
